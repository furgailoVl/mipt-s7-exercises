from re import S
from typing import Tuple
from dashboard.index import app

import numpy as np
import plotly.graph_objects as go
from dash.dependencies import Input, Output, State


def get_normal_distribution(median: float, standart_deviaton: float) -> Tuple:
    '''
    Calculate https://en.wikipedia.org/wiki/Normal_distribution 
    '''
    #TODO: ADD CALCULATION FUNCTION

    


@app.callback(
    Output(component_id='bar_plot', component_property='figure'),
    #TODO ADD INPUTS AND STATE
    )   
def graph_update(m, d, _):
    if m is None or d is None:
        m = 0.0
        d = 0.2

    x_data, y_data = get_normal_distribution(m, d)

    fig = go.Figure([go.Scatter(x = x_data, y = y_data,\
                     line = dict(color = 'mediumseagreen', width = 2), name="data_type", 
                     )],)
    
    fig.update_layout(title = 'Normal distribution',
                      xaxis_title = 'x',
                      yaxis_title = 'f(x)'
                      )
    return fig  