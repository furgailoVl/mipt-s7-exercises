from dash import html, dcc


def get_main_page():
    return html.Div(id = 'parent', 
        children = [
            html.H1(id = 'H1', children = 'Normal distribution calculator', style = {'textAlign':'center',\
                                                'marginTop':40,'marginBottom':40}),

            html.Div(id = 'text-boxes', 
                children =  [
                    dcc.Input(
                    id='median',
                    type='number',
                    placeholder='Pass a median',
                    style={'width': '100%', 'height': 30},
                    ),
                    dcc.Input(
                    id='deviation',
                    type='number',
                    placeholder='Pass a standart deviation',
                    style={'width': '100%', 'height': 30},
                    ),
        
                    html.Button('Draw', id='draw-button', n_clicks=0), 

                    dcc.Graph(id = 'bar_plot')
                ]
            )
        ])
